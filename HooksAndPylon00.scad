/* HooksAndPylon00.scad
 * 2018 06 11
 * gratefulfrog.org
 * generates pylon and hooks for Freewing Stores pod
 * credit to https://www.thingiverse.com/apmech1/about
 * for his model: https://www.thingiverse.com/thing:2858226/
 * Usage:
 * at the very end of the file, uncomment the lines needed
 */

$fn=100;

wallThickness = 2;

// hook measurements
hookSpacingY  = 50;
hookLength    = 4.76;
BottomWidth      = 1.58 ;
FirstVertical    = 2.39;
bottomWingWidth  = 1.58;
_2ndVertical     = 0.78;
_3rdVertical     = _2ndVertical+0.01;
TopWidth         = 3.18;

hookSupportX = BottomWidth  + 2*bottomWingWidth;
hookSupportY = hookSpacingY + hookLength;

////////////////////////////////////////////////////////////////////////////
/////////////////////////  HOOKS ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

module measuredHook(wallThickness){
  epsilon= wallThickness;  // to ensure ancoring in the base!
  points = [[0,-epsilon],
            [BottomWidth,-epsilon],
            [BottomWidth,FirstVertical],
            [BottomWidth+bottomWingWidth,FirstVertical],
            [BottomWidth+bottomWingWidth,FirstVertical+_2ndVertical],
            [BottomWidth+bottomWingWidth-_3rdVertical,FirstVertical+_2ndVertical+_3rdVertical],
            [BottomWidth+bottomWingWidth-_3rdVertical-TopWidth,FirstVertical+_2ndVertical+_3rdVertical],
            [-BottomWidth,FirstVertical+_2ndVertical],
            [-BottomWidth,FirstVertical],
            [0,FirstVertical]];
  translate([-BottomWidth/2.0,hookLength,0])
  rotate([90,0,0])
    linear_extrude(height = hookLength)
      polygon(points,convexity = 1);
}
module twoMeasuredHooks(wallThickness,withSupport){
  hookSupportZ = wallThickness;
  translate([0,
             -hookSupportY/2.,
             0]){
    measuredHook(wallThickness);
    translate([0,hookSpacingY,0])
      measuredHook(wallThickness);
  }
  if (withSupport){
    hookSupport(wallThickness);
  }
}
module hookSupport(wallThickness){
  hookSupportZ = wallThickness;
  translate([-hookSupportX/2.,-hookSupportY/2.,-hookSupportZ])
          cube([hookSupportX,hookSupportY,hookSupportZ]);
}
////////////////////////////////////////////////////////////////////////////
/////////////////////////  end of HOOKS ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
/////////////////////////  PYLON ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

pylonHeight = 30;
pylonWidth  = 8;
pylonLenghtCenter2Center = 81;

pylonBaseHeight = 6.5;
pylonTopHeight = 5;
pylonMiddleHeight=pylonHeight-pylonBaseHeight-pylonTopHeight;

// top to middle algnment pins
pinDiameter = pylonWidth/2.;
pinOverlap = 2;
pinHeight = pylonTopHeight + pinOverlap;
pinSlop = 0.3;

module slots(){
  difference(){
    translate([0,2.5,0])
      rotate([0,0,90])
        import("Freewing_Harpoint_Attachment_Hardware/files/FW_Female_Attachment.stl");
    union(){
      translate([9,0,0])
        cube([10,100,10],center=true);
      translate([-9,0,0])
        cube([10,100,10],center=true);
      translate([-5,37.3,0])
        cube([10,20,10]);
    }
  }
}
module pylonBase0(dia,ht,separation){
  translate([0,separation/2.,0]){
    translate([-dia/2.,-dia/2.,0])
      cube([dia,dia/2.,ht]);
    cylinder(d=dia,h=ht);
  }
  translate([0,-separation/2.,0]){
    translate([-dia/2.,0,0])
      cube([dia,dia/2.,ht]);
    cylinder(d=dia,h=ht); 
  } 
  slots();
}
module pylonBase(){
  pylonBase0(pylonWidth,pylonBaseHeight,pylonLenghtCenter2Center);
}
module pylonMiddle(){
  pylonMiddle0(pylonWidth,
               pylonMiddleHeight,
               pylonLenghtCenter2Center,
               wallThickness);
}
module pins0(pinHt,pinDia,pinSeparation,overlap,slop=0){
  translate([0,pinSeparation/2.,-overlap])
    cylinder(d=pinDia+slop,h=pinHt);
  translate([0,-pinSeparation/2.,-overlap])
    cylinder(d=pinDia+slop,h=pinHt);
}
module pins(slop=0){
  pins0(pinHeight,pinDiameter,pylonLenghtCenter2Center,pinOverlap,slop);
}
module pylonMiddle0(dia,midHt,separation,wall){
  difference(){
    hull(){
       translate([0,separation/2.,0]){
        cylinder(d=dia,h=midHt);
      }
      translate([0,-separation/2.,0]){
        cylinder(d=dia,h=midHt); 
      } 
    }
  translate([0,0,midHt/2.])
    cube([dia-wall,
          separation-dia,
          midHt],center=true);
  }
  translate([0,0,midHt])
    pins();
}
module buildPylon0(width,lenCenter2Center,baseHt,midHt,topHt,wall){
  pylonBase0(width,baseHt,lenCenter2Center);
  translate([0,0,baseHt])
    pylonMiddle0(width,midHt,lenCenter2Center,wall);
}
module buildPylonLower(){
  buildPylon0(pylonWidth,
              pylonLenghtCenter2Center,
              pylonBaseHeight,
              pylonMiddleHeight,
              pylonTopHeight,
              wallThickness);
}
module pylonTop0(dia,topHt,separation,topZ,hooks=true){
  translate([0,0,topZ])
    hull(){
       translate([0,separation/2.,0]){
        cylinder(d=dia,h=topHt);
      }
      translate([0,-separation/2.,0]){
        cylinder(d=dia,h=topHt); 
      } 
    }
    if(hooks){
      translate([0,0,pylonHeight])
        twoMeasuredHooks(wallThickness, false);
    }
}
module pylonTop1(slop){
  difference(){
    pylonTop0(pylonWidth,
              pylonTopHeight,
              pylonLenghtCenter2Center,
              pylonBaseHeight + pylonMiddleHeight);
    translate([0,0,pylonMiddleHeight+pylonBaseHeight])
        pins(slop);
  }
}
module buildPylonUpper(){
  pylonTop1(pinSlop);
}

////////////////////////////////////////////////////////////////////////////
/////////////////////////  end of PYLON ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//translate([0,0,2])
//buildPylonUpper();
//buildPylonLower();
//twoMeasuredHooks(wallThickness, false);
 /*
pylonTop0(pylonWidth,
              pylonTopHeight,
              0.7*pylonLenghtCenter2Center,
              pylonBaseHeight + pylonMiddleHeight);
*/