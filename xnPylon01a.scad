// xnPylon.scad
// 2018 06 17
// Gratefulfrog
// Good to go, yet...
///////////////////////////////////
// Creative Commons - Attribution
// this file uses:
//Freewing Harpoint Attachment Hardware by apmech1
//Published on April 12, 2018
//www.thingiverse.com/thing:2858226
///////////////////////////////////

include <HooksAndPylon00.scad>

$fn=100;

totalZ = 30; //15;
slotFixY = 10;

slotTargetX = 8;
slotTargetY = 55+slotFixY;
slotTargetZ = 5;

slotTargetOffsetY = 2.5;

slotCutterX = 2*slotTargetX;
slotCutterY = 2*slotTargetY;
slotCutterZ = 2*slotTargetZ;

module slotCutterBlock(x=slotCutterX,y=slotCutterY,z=slotCutterZ){
  cube([x,y,z],center=true);
}
module getSlots(){
  // this is what we got
  translate([0,-slotTargetOffsetY,0])
    rotate([0,0,90])
      import("Freewing_Harpoint_Attachment_Hardware/files/FW_Female_Attachment.stl");
}

module slotTarget(){
  // this is what we got
  translate([0,0,-slotTargetZ/2.])
    difference(){
      getSlots();
      union(){
        translate([0,0,slotTargetZ+slotCutterZ/2.])
          slotCutterBlock();
        translate([slotTargetX/2.+slotCutterX/2.,0,0])
          slotCutterBlock();
        translate([-(slotTargetX/2.+slotCutterX/2.),0,0])
          slotCutterBlock();
        translate([0,-(slotTargetY/2.+slotCutterY/2.),slotCutterZ/4.])
          slotCutterBlock();
        translate([0,(slotTargetY/2.+slotCutterY/2.),slotCutterZ/4.])
          slotCutterBlock();
      }
    }
}



vWallX = 1;
hWallZ = 1.5;
forwardHoleX = slotTargetX - 2*vWallX;
forwardHoleY = 4*forwardHoleX;
forwardHoleZ = slotTargetZ;// - hWallZ;

module forwardHole(x=forwardHoleX,y=forwardHoleY,z=forwardHoleZ,
                   stY=slotTargetY){
  translate([0,-(y/2.+stY/2.),0])
    cube([x,y,z],center=true);
}
//translate([0,0,10])
  //slotSpace();
//slotTarget();
//forwardHole();
//slotSpace();
//translate([0,0,-slotTargetZ/2.])
//#xnPylon();
//translate([0,0,totalZ-slotTargetZ])
  //            slotSpace(slotTargetX,slotTargetY,slotTargetZ);
/*
%difference(){
          union(){
            slotSpace(slotTargetX,slotTargetY,slotTargetZ);
            translate([0,0,totalZ-slotTargetZ])
              slotSpace(slotTargetX,slotTargetY,slotTargetZ);
            translate([0,slotTargetY/2.,slotTargetZ/2.])
              cylinder(d=slotTargetX,h=totalZ-2*slotTargetZ);
            translate([0,-slotTargetY/2.,slotTargetZ/2.])
              cylinder(d=slotTargetX,h=totalZ-2*slotTargetZ);
          }
          union(){
            forwardHole();
            slotSpace();
          }
        } 
*/
module slotSpace(x=slotTargetX,y=slotTargetY,z=slotTargetZ){
  cube([x,y,z],center=true);
}
module xnPylon(){
  translate([0,0,slotTargetZ/2.]){
    union(){
      scale([0.999999,0.999999,0.999999]){
        difference(){
          hull(){
            slotSpace(slotTargetX,slotTargetY,slotTargetZ);
            translate([0,0,totalZ-slotTargetZ])
              slotSpace(slotTargetX,slotTargetY,slotTargetZ);
            translate([0,slotTargetY/2.,slotTargetZ/2.])
              cylinder(d=slotTargetX,h=totalZ-2*slotTargetZ);
            translate([0,-slotTargetY/2.,slotTargetZ/2.])
              cylinder(d=slotTargetX,h=totalZ-2*slotTargetZ);
          }
          union(){
            forwardHole();
            slotSpace();
          }
        } 
      }
      slotTarget();
      translate([0,0,totalZ-slotTargetZ/2.])
        twoMeasuredHooks(2,false);  
    }
  }
}


module pins(slop=0,x=slotTargetX,y=slotTargetY,z=slotTargetZ,tz=totalZ,ytFactor=3.33,yTrans=0){
  dia = x/3.+slop;
  ht= z;
  yt = slotTargetY/ytFactor;
  translate([0,yTrans+yt,tz-ht])
    cylinder(d=dia,h=ht);
  translate([0,yTrans-yt,tz-ht])
    cylinder(d=dia,h=ht);
}

module bigC(targetH,positive=1,x=slotTargetX*2,y=slotTargetY*2,z=slotTargetZ*10){
  translate([0,0,targetH-z/2.*positive])
    cube([x,y,z],center=true);
}

module xnPylonUpper(topZ=slotTargetZ,slop=0.3){
  difference(){
    xnPylon();
    union(){
      pins(slop);
      bigC(totalZ-topZ);
    }
  }
}
module xnPylonLower(topZ=slotTargetZ,slopP=0.3){
  difference(){
    xnPylon();
    union(){
      bigC(totalZ-topZ,-1);
      creuseur();
      xnPylonLowest();
      pins(slop=slopP,tz=1.5*slotTargetZ,ytFactor=6,yTrans=-1.5*slotTargetOffsetY);
    }
  }
  pins();
}
module xnPylonLowest(topZ=slotTargetZ){
  difference(){
    xnPylon();
    union(){
      bigC(slotTargetZ*0.999,-1);
      creuseur();
    }
  }
    pins(tz=1.5*slotTargetZ,ytFactor=6,yTrans=-1.5*slotTargetOffsetY);
}

module creuseur(x=slotTargetX-2*wallThickness,y=slotTargetY/2.,z=totalZ){
  translate([0,0,z/2.+slotTargetZ])
    cube([x,y,z],center=true);
}

translate([0,0,20])
  xnPylonUpper(); 

translate([0,0,10])
  xnPylonLower();

xnPylonLowest();

