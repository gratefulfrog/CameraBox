// xnPylon.scad
// 2018 06 17
// Gratefulfrog
// Good to go, yet...
///////////////////////////////////
// Creative Commons - Attribution
// this file uses:
//Freewing Harpoint Attachment Hardware by apmech1
//Published on April 12, 2018
//www.thingiverse.com/thing:2858226
///////////////////////////////////


include <HooksAndPylon00.scad>

$fn=100;

totalZ = 30; //15;

slotTargetX = 8;
slotTargetY = 55;
slotTargetZ = 6;

slotTargetOffsetY = -2.5;

slotCutterX = 2*slotTargetX;
slotCutterY = 2*slotTargetY;
slotCutterZ = 2*slotTargetZ;

module slotCutterBlock(x=slotCutterX,y=slotCutterY,z=slotCutterZ){
  cube([x,y,z],center=true);
}
module slotCible(){
  // this is what we think we want
  cube([slotTargetX,slotTargetY,slotTargetZ],center=true);
}
module getSlots(){
  // this is what we got
  translate([0,-slotTargetOffsetY,0])
    rotate([0,0,90])
      import("Freewing_Harpoint_Attachment_Hardware/files/FW_Female_Attachment.stl");
}
aftSlotOffsetY = -13.955;
module singleSlot(sign){
  cX = 20;
  cY = 100;
  cZ = 40;
  difference(){
    getSlots();
    translate([0,sign*cY/2.,cZ/2.])
      cube([cX,cY,cZ],center=true);
  }
}
module getAdjustedSlots(aftOffsetY=aftSlotOffsetY){
  singleSlot(1);
  translate([0,aftOffsetY,0])
  singleSlot(-1);
}
module slotTarget(){
  // this is what we got
  translate([0,0,-slotTargetZ/2.])
    difference(){
      getAdjustedSlots();
      union(){
        translate([0,0,slotTargetZ+slotCutterZ/2.])
          slotCutterBlock();
        translate([slotTargetX/2.+slotCutterX/2.,0,0])
          slotCutterBlock();
        translate([-(slotTargetX/2.+slotCutterX/2.),0,0])
          slotCutterBlock();
        translate([0,-(slotTargetY/2.+slotCutterY/2.),slotCutterZ/4.])
          slotCutterBlock();
        translate([0,(slotTargetY/2.+slotCutterY/2.),slotCutterZ/4.])
          slotCutterBlock();
      }
    }
}



vWallX = 1;
hWallZ = 1.5;
forwardHoleX = slotTargetX - 2*vWallX;
forwardHoleY = 4*forwardHoleX;
forwardHoleZ = slotTargetZ;// - hWallZ;

module forwardHole(x=forwardHoleX,y=forwardHoleY,z=forwardHoleZ,
                   stY=slotTargetY){
  translate([0,-(y/2.+stY/2.),0])
    cube([x,y,z],center=true);
}

module slotSpace(x=slotTargetX,y=slotTargetY,z=slotTargetZ){
  cube([x,y,z],center=true);
}

module xnPylon(){
  translate([0,0,slotTargetZ/2.]){
    union(){
      scale([0.999999,0.999999,0.999999]){
        difference(){
          hull(){
            slotCible(slotTargetX,slotTargetY,slotTargetZ);
            translate([0,0,totalZ-slotTargetZ])
              slotSpace(slotTargetX,slotTargetY,slotTargetZ);
            translate([0,slotTargetY/2.,slotTargetZ/2.])
              cylinder(d=slotTargetX,h=totalZ-2*slotTargetZ);
            translate([0,-slotTargetY/2.,slotTargetZ/2.])
              cylinder(d=slotTargetX,h=totalZ-2*slotTargetZ);
          }
          union(){
            forwardHole();
            slotCible();
          }
        } 
      }
      slotTarget();
      translate([0,0,totalZ-slotTargetZ/2.])
        twoMeasuredHooks(2,false);  
    }
  }
}


module pins(slop=0,x=slotTargetX,y=slotTargetY,z=slotTargetZ,tz=totalZ){
  dia = x/3.+slop;
  ht= z;
  yt = slotTargetY/3.;
  translate([0,yt,tz-ht])
    cylinder(d=dia,h=ht);
  translate([0,-yt,tz-ht])
    cylinder(d=dia,h=ht);
}

module bigC(targetH,positive=1,x=slotTargetX*2,y=slotTargetY*2,z=slotTargetZ*10){
  translate([0,0,targetH-z/2.*positive])
    cube([x,y,z],center=true);
}

module xnPylonUpper(topZ=slotTargetZ/2.,slop=0.3){
  difference(){
    xnPylon();
    union(){
      pins(slop);
      bigC(totalZ-topZ);
    }
  }
}
module xnPylonLower(topZ=slotTargetZ/2.,slop=0.3){
  difference(){
    xnPylon();
    union(){
      bigC(totalZ-topZ,-1);
      creuseur();
    }
  }
  pins();
}

module creuseur(x=slotTargetX-2*wallThickness,y=slotTargetY/2.,z=totalZ){
  translate([0,0,z/2.+slotTargetZ])
    cube([x,y,z],center=true);
}

//translate([0,0,10])
//xnPylonUpper(); 
//xnPylonLower();