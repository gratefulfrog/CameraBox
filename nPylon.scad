// nPylon.scad
// 2018 06 15
// Gratefulfrog
// not quite good to go, yet...

$fn=100;

include <HooksAndPylon00.scad>

/////////////////////////////////////////////////////////////////////
////////////////////// SLOT BLOCK  //////////////////////////////////
/////////////////////////////////////////////////////////////////////
rampHeight = 2.5;
slotWidth = 8;
slotLength = 64.8;
module slotCutterZ(ht=10){
  translate([0,0,2*rampHeight+ht/2.])
    cube([12,100,ht],center=true);
}
module slotCutterFront(ly=50,ht=rampHeight*5){
  translate([0,-27.5-ly/2.,ht/2])
    cube([10,ly,ht],center = true);
}
module slotTop(){
  translate([0,4.9,2*rampHeight])
  cube([slotWidth,slotLength,wallThickness], center=true);
}
module slotBlock(){
  difference(){
    union(){
      slots();
      slotTop();
    }
    union(){
      slotCutterZ();
      slotCutterFront();
    }
  }
}
/////////////////////////////////////////////////////////////////////
//////////////////////   nPylon    //////////////////////////////////
/////////////////////////////////////////////////////////////////////

R = 80; 
D = 8; 
angle = 5;
H = tan(angle)*(2*R + D); 

alpha = atan2(H/2,(R+D/2.));

module arc0(eps=0,RR = R-D,aa = 30,bb= 30){
  //RR = R-D;  // big radius
  // aa = 30;  // arc length
  // bb= 30;   // arc rotation/shift
  delta =sin(bb); 
  translate([0,0,RR/2])
    rotate([0,-90,0])
      rotate([0,0,bb])
        translate([-RR,0,0])
          union(){
            rotate_extrude(angle=aa)
              translate([RR,0,0])
                circle(d=D+eps);
            translate([RR,0,0])
              sphere(d=D+eps);
            rotate([0,0,aa])
              translate([RR,0,0])
                sphere(d=D+eps);
          }
 }
module bow(eps=0){
  translate([0,-37.5,5])
     arc0(eps,RR=50,aa=35,bb=45);
}
module stern(eps=0){
  translate([0,25,3])
     mirror([0,1,0])
      arc0(eps,RR=50,aa=35,bb=95);
}
module insider(h=30-2*rampHeight-2){
  
  translate([0,-5,h/2+2*rampHeight])
  cube([D-2*wallThickness,40,h],center=true);
}
module nicePylon(){
  difference(){
    hull(){
      bow();
      stern();
    }
    union(){
      hull()  
        bow(0.1);
      hull()
        stern(0.1);
    }
  }
  bow(0.1);
  stern(0.1);
}
module nPylon(){
  cht= 20;
  difference(){
    nicePylon();
  union(){
    translate([0,0,30+cht/2])
        cube([20,150,cht],center=true);
    translate([0,0,+2*rampHeight-cht/2-0.1])
        cube([20,150,cht],center=true);
    insider();
    }
  }
}

module allCutter(h=25){
  translate([0,0,h/2])
    cube([10,200,h],center=true);
}
module nPylonFull(){
  slotBlock();
  translate([0,0,30])
  twoMeasuredHooks(2,true);
  nPylon();    
}
module nPylonLower(){
  difference(){
    nPylonFull();
    translate([0,0,25])
      allCutter();
  }
  translate([0,-5,25])
    pins0(pinHeight,pinDiameter,50,pinOverlap,slop=0);
}
module nPylonUpper(){
  difference(){
    nPylonFull();
    union(){
      translate([0,0,0])
        allCutter();
      translate([0,-5,25])
        pins0(pinHeight,pinDiameter,50,pinOverlap,slop=pinSlop);
    }
  }
}
translate([0,0,5])
  nPylonUpper();
nPylonLower();
//slotBlock();


