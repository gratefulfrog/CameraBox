$fn=100;

module vert(wireD,h,space){
  linear_extrude(height =h,center=true){
    for (x=[-h/2:space:h/2]){
      translate([x,0,0])
        circle(d=wireD);
    }
  }
}
module grid(wireD,h,space){
vert(wireD,h,space);
rotate([0,90,0])
  vert(wireD,h,space);
}

//grid(0.5,20,2);

module arches(){
  linear_extrude(height=0.5,center = true)
    difference(){
      for (ra=[0.5:1:10]){
        difference(){
          circle (r=ra);
          circle(r=ra-0.5);
        }
      }
        translate([-10,-10,0])
      square([10,20]);
    }
  }
  
  arches();
  
rotate([0,180,0])
  translate([-9.5,0,0])
      arches();

  